import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'

Vue.config.productionTip = false
Vue.use(VueRouter);
Vue.use(ElementUI);

import StudentList from './components/Students/StudentList'
import Add from './components/add/Add'

var router = new VueRouter({
    routes:[
        {path:"/student",component:StudentList},
        {path:"/add/:id",component:Add},
        {path:"/add",component:Add}
    ]
})

new Vue({
  render: h => h(App),
    router
}).$mount('#app')
