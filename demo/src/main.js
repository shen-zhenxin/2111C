import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(ElementUI);

import add from './components/add/add'

var router = new VueRouter({
    mode:'history',
    routes:[
        {path:"/add",component:add},
        {path:"/add/:id",component:add},
    ]
})

new Vue({
  render: h => h(App),
   router
}).$mount('#app')
