package com.usian.pojo;

public class RedisDto {
    private String value;
    private Integer score;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public RedisDto() {
    }

    public RedisDto(String value, Integer score) {
        this.value = value;
        this.score = score;
    }
}
