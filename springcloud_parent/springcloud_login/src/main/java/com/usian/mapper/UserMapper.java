package com.usian.mapper;


import com.usian.pojo.Logins;
import com.usian.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

@Mapper
public interface UserMapper extends tk.mybatis.mapper.common.Mapper<User> {

    User findByName(Logins logins);

    @Update("update `user` set loginum=#{loginum} where name=#{name}")
    void addNum(User user);

    User isHave(Logins logins);

    @Update("update `user` set lastdate = #{lastdate} where name=#{name}" )
    void updateLast(User user);

    @Update("update `user` set loginum = 0")
    void upAll();

}
