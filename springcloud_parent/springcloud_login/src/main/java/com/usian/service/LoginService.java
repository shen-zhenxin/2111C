package com.usian.service;

import com.usian.pojo.Logins;
import com.usian.pojo.User;

import java.util.Date;

public interface LoginService {
    User Login(Logins logins);

    void addNum(User byName);

    User isHave(Logins logins);

    void last(User user);

    void setAll();

}
