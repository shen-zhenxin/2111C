package com.usian.service.impl;

import com.usian.mapper.UserMapper;
import com.usian.pojo.Logins;
import com.usian.pojo.User;
import com.usian.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginImpl implements LoginService {

    @Autowired
    private UserMapper um;

    @Override
    public User Login(Logins logins) {
        return um.findByName(logins);
    }

    @Override
    public void addNum(User user) {
        um.addNum(user);
    }

    @Override
    public User isHave(Logins logins) {
        User user = um.isHave(logins);
        return user;
    }

    @Override
    public void last(User user) {
        um.updateLast(user);
    }

    @Override
    public void setAll() {
        um.upAll();
    }
}
