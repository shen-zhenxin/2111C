package com.usian.task;

import com.usian.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserTask {
    @Autowired
    private LoginService ls;

    @Scheduled(cron = "0 0 0 * * ?")
    public void taskUser(){
        Logger logger = LoggerFactory.getLogger(UserTask.class);
        ls.setAll();
        Date date = new Date();
        logger.info(date+"加载");
    }

}
