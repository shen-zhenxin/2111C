package com.usian.controller;

import com.usian.entity.Result;
import com.usian.pojo.Logins;
import com.usian.pojo.User;
import com.usian.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("login")
public class LoginController {

    @Autowired
    private LoginService ls;

    @RequestMapping("in")
    public Result in(@RequestBody Logins logins){
        User have = ls.isHave(logins);
        if (have!=null){
            User login = ls.Login(logins);
            if (login==null){
                User user = new User();
                user.setId(have.getId());
                user.setName(have.getName());
                user.setPhone(have.getPhone());
                user.setFirstenrol(have.getFirstenrol());
                user.setLastdate(have.getLastdate());
                user.setLoginum(have.getLoginum()+1);
                ls.addNum(user);
                return new Result(false,"密码错误!!!");
            }else {
                if (login.getLoginum()>=3){
                    return new Result(false,"今日该用户登录次数过多!");
                }else {
                    Date date = new Date();
                    login.setLastdate(date);
                    ls.last(login);
                    return new Result(true,"登录成功!");
                }
            }
        }else {
            return new Result(false,"用户名不存在!");
        }
    }
}
