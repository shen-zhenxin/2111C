package com.usian.pojo;

import java.util.Date;

public class User {
    private Integer id;
    private String name;
    private String phone;
    private Date firstenrol;
    private Date lastdate;
    private Integer loginum;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getFirstenrol() {
        return firstenrol;
    }

    public void setFirstenrol(Date firstenrol) {
        this.firstenrol = firstenrol;
    }

    public Date getLastdate() {
        return lastdate;
    }

    public void setLastdate(Date lastdate) {
        this.lastdate = lastdate;
    }

    public Integer getLoginum() {
        return loginum;
    }

    public void setLoginum(Integer loginum) {
        this.loginum = loginum;
    }
}
