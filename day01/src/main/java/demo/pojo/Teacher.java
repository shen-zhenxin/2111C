package demo.pojo;

import java.util.Date;

public class Teacher {
    private Integer id;
    private Date aa;

    public Date getAa() {
        return aa;
    }

    public void setAa(Date aa) {
        this.aa = aa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
