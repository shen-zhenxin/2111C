package demo.service;

import demo.pojo.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    void deleteUserById(Integer id);

    User findUserById(Integer id);

    void add(User user);

    void edit(User user);
}
