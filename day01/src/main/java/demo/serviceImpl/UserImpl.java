package demo.serviceImpl;

import demo.mapper.UserMapper;
import demo.pojo.User;
import demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserImpl implements UserService {

    @Autowired
    private UserMapper um;

    @Override
    public List<User> findAll() {
        return um.findAll();
    }

    @Override
    public void deleteUserById(Integer id) {
        um.delete(id);
    }

    @Override
    public User findUserById(Integer id) {
        return um.findById(id);
    }

    @Override
    public void add(User user) {
        um.add(user);
    }

    @Override
    public void edit(User user) {
        um.edit(user);
    }
}
