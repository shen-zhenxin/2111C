package demo.mapper;

import demo.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select * from user")
    List<User> findAll();

    @Select("select * from user where id=#{id}")
    User findById(Integer id);

    @Delete("delete from user where id=#{id}")
    void delete(Integer id);

    @Insert("insert into user values (null,#{name},#{birthday})")
    void add(User user);

    @Update("update user set name=#{name},birthday=#{birthday} where id=#{id}")
    void edit(User user);
}
