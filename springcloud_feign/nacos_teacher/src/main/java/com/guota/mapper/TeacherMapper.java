package com.guota.mapper;

import com.guota.pojo.Teacher;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface TeacherMapper extends Mapper<Teacher> {
    @Update("update teacher set sid=#{sid} where id=#{id}")
    void up(Teacher teacher);
}
