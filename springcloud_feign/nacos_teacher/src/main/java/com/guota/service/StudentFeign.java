package com.guota.service;

import com.guota.pojo.Student;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("nacos-student")
public interface StudentFeign {

    @PostMapping("/student/add")
    public String add(@RequestBody Student student);

    @GetMapping("/student/edit/{id}")
    public String edit(@PathVariable Integer id);

    @GetMapping("/student/findAll/{id}")
    public Student findAll(@PathVariable Integer id);
}
