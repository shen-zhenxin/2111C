package com.guota.service;

import com.guota.pojo.Teacher;

public interface TeacherService {
    void editTea(Teacher teacher);

    Teacher findOneTea(Integer id);

    void insertTea(Teacher teacher);

    Teacher findById(Integer id);

    void del(Integer id);
}
