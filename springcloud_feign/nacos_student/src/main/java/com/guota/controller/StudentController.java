package com.guota.controller;

import com.guota.pojo.Student;
import com.guota.pojo.Teacher;
import com.guota.service.StudentService;
import com.guota.service.TeacherFeign;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import sun.reflect.generics.tree.ReturnType;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private TeacherFeign tf;

    @Autowired
    private StudentService studentService;

    @PostMapping("/add")
    public String add(@RequestBody Student student){
        studentService.insertStu(student);

        Teacher teacher = tf.findOne(1);

        Integer sid = student.getId();
        String sid1 = teacher.getSid();
        Teacher teacher1=new Teacher();
        teacher1.setId(1);
        teacher1.setSid(sid1+","+sid);
        tf.editTea(teacher1);
        return "成功!";
    }
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable Integer id){
        studentService.findByTid(id);
        return "成功!";
    }
    @GetMapping("/findAll/{id}")
    public Student findAll(@PathVariable Integer id){
        return studentService.findAll(id);
    }

}
