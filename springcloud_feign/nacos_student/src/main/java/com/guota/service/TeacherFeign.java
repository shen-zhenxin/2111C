package com.guota.service;

import com.guota.pojo.Teacher;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient("nacos-teacher")
public interface TeacherFeign {
    @GetMapping("/teacher/findOne/{id}")
    public Teacher findOne(@PathVariable Integer id);
    @PostMapping("/teacher/edit")
    public String editTea(@RequestBody Teacher teacher);
    @PostMapping("/teacher/insert")
    public String insert(@RequestBody Teacher teacher);
    @DeleteMapping("/teacher/del/{id}")
    public String del(@PathVariable Integer id);
    @GetMapping("/teacher/findAll/{id}")
    public Teacher findAll(@PathVariable Integer id);
}
