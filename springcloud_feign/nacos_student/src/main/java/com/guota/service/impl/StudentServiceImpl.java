package com.guota.service.impl;

import com.guota.mapper.StudentMapper;
import com.guota.pojo.Student;
import com.guota.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;
    @Override
    public void insertStu(Student student) {
        studentMapper.insertSelective(student);
    }


    @Override
    public void findByTid(Integer id) {
            studentMapper.edit(id);
    }

    @Override
    public Student findAll(Integer id) {
        return studentMapper.selectByPrimaryKey(id);
    }

}
