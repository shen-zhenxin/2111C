package com.guota.service;

import com.guota.pojo.Student;

import java.util.List;

public interface StudentService {
    void insertStu(Student student);

    void findByTid(Integer id);

    Student findAll(Integer id);

}
