package demo.controller;

import demo.entity.PageResult;
import demo.entity.QueryPageBean;
import demo.entity.Result;
import demo.pojo.People;
import demo.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("people")
public class PeopleController {

    @Autowired
    private PeopleService ps;

    @RequestMapping("findAll")
    @ResponseBody
    public PageResult findAll(@RequestBody QueryPageBean queryPageBean){
        return ps.findAll(queryPageBean);
    }

    @RequestMapping("add")
    @ResponseBody
    @CrossOrigin(origins = "*")
    public Result add(@RequestBody People people){
        System.out.println(people+"-------");
        if (people.getId()!=null){
            ps.edit(people);
            return new Result(true,"修改成功!");
        }else {
            ps.add(people);
            return new Result(true,"添加成功!");
        }
    }


    @RequestMapping("del")
    @ResponseBody
    @CrossOrigin(origins = "*")
    public Result del(Integer id){
        ps.del(id);
        try {
            return new Result(true,"成功!");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"失败!");
        }
    }

    @RequestMapping("findById")
    @ResponseBody
    @CrossOrigin(origins = "*")
    public People findById(Integer id){
        return ps.findById(id);
    }
}
