package demo.controller;

import demo.entity.PageResult;
import demo.entity.QueryPageBean;
import demo.entity.Result;
import demo.pojo.Provice;
import demo.pojo.Users;
import demo.service.UserSerive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UsersController {

    @Autowired
    private UserSerive us;
    
    @RequestMapping("findAll")
    public PageResult findAll(@RequestBody QueryPageBean queryPageBean){
        return us.findAll(queryPageBean);
    }

    @RequestMapping("add")
    @CrossOrigin(origins = "*")
    public Result add(@RequestBody Users users){
        if (users.getId()!=null){
            us.edit(users);
            try {
                return new Result(true,"修改成功!");
            } catch (Exception e) {
                e.printStackTrace();
                return new Result(false,"失败");
            }
        }else {
            us.add(users);
            try {
                return new Result(true,"添加成功!");
            } catch (Exception e) {
                e.printStackTrace();
                return new Result(false,"失败");
            }
        }
    }

    @GetMapping("findPCC")
    @CrossOrigin(origins = "*")
    public List<Provice> findCounty(Integer id){
        return us.findPCC(id);
    }


    @GetMapping("findParent")
    @CrossOrigin(origins = "*")
    public Integer findParent(Integer id){
        return us.findParent(id);
    }




    @RequestMapping("del")
    @CrossOrigin(origins = "*")
    public Result del(Integer[] ids){
        us.del(ids);
        try {
            return new Result(true,"成功!");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"失败!");
        }
    }

    @RequestMapping("findById")
    @CrossOrigin(origins = "*")
    public Users findById(Integer id){
        return us.findById(id);
    }

}
