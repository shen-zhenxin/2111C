package demo.controller;

import demo.entity.Result;
import demo.pojo.Student;
import demo.pojo.Teacher;
import demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("student")
@Controller
public class StudentController {

    @Autowired
    private StudentService ss;

    @PostMapping("add")
    @ResponseBody
    public Result add(Integer[] ids,@RequestBody Teacher teacher){
            ss.add(teacher,ids);
        return new Result(true,"成功!");
    }

    @RequestMapping("find")
    @ResponseBody
    public List<Student> findStudent(){
        return ss.findStudent();
    }

}
