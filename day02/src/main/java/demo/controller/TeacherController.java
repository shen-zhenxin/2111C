package demo.controller;

import com.github.pagehelper.PageInfo;
import demo.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("teacher")
public class TeacherController {

    @Autowired
    private TeacherService ts;

    @RequestMapping("findAll")
    @ResponseBody
    public PageInfo findAll(Integer pageIndex,@RequestBody Map map){
       return ts.findAll(pageIndex,map);
    }

}
