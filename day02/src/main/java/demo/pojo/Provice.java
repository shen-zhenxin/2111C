package demo.pojo;

import lombok.Data;

@Data
public class Provice {
    private Integer id;
    private String name;
    private Integer parentId;
}
