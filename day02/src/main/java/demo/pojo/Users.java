package demo.pojo;


import java.util.Date;

public class Users {
    private Integer id;
    private String name;
    private Date birthday;
    private Integer provinceId;
    private String address;
    private Integer province;
    private Integer city;
    private Integer county;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String phone;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getCounty() {
        return county;
    }

    public void setCounty(Integer county) {
        this.county = county;
        this.setProvinceId(county);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", provinceId=" + provinceId +
                ", address='" + address + '\'' +
                ", province=" + province +
                ", city=" + city +
                ", county=" + county +
                '}';
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }
}
