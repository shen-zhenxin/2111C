package demo.pojo;

import lombok.Data;

import java.util.Objects;

@Data
public class Student {
    private Integer sid;
    private String sname;
    private Integer show;
    private Integer tid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(sid, student.sid) &&
                Objects.equals(sname, student.sname) &&
                Objects.equals(show, student.show) &&
                Objects.equals(tid, student.tid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sid, sname, show, tid);
    }
}
