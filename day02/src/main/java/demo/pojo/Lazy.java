package demo.pojo;

public class Lazy {
    private String name;
    private Lazy(){}
    private static Lazy lazy = null;
    public static synchronized Lazy getLazy(){
        if (lazy==null){
            lazy = new Lazy();
        }
        return lazy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Lazy{" +
                "name='" + name + '\'' +
                '}';
    }
}
