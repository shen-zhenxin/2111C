package demo.serviceImpl;

import demo.mapper.StudentMapper;
import demo.mapper.TeacherMapper;
import demo.pojo.Student;
import demo.pojo.Teacher;
import demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class StudentImpl implements StudentService {

    @Autowired
    private TeacherMapper tm;

    @Autowired
    private StudentMapper sm;

    @Override
    public void add(Teacher teacher, Integer[] ids) {
        tm.insertSelective(teacher);
        System.out.println("-------------------------"+teacher.getId()+"-------------------------");
        if (ids!=null){
            for (Integer sid : ids) {
                sm.set(sid,teacher.getId());
            }
        }
    }

    @Override
    public List<Student> findStudent() {
        return sm.findStudent();
    }
}
