package demo.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import demo.mapper.TeacherMapper;
import demo.pojo.Dto;
import demo.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TeacherImpl implements TeacherService {

    @Autowired
    private TeacherMapper tm;

    @Override
    public PageInfo findAll(Integer pageIndex, Map map) {
        PageHelper.startPage(pageIndex,2);
        List<Dto> all = tm.findAll(map);
        return new PageInfo(all);
    }

}
