package demo.serviceImpl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import demo.entity.PageResult;
import demo.entity.QueryPageBean;
import demo.mapper.ProviceMapper;
import demo.mapper.UserMapper;
import demo.pojo.People;
import demo.pojo.Provice;
import demo.pojo.Users;
import demo.service.UserSerive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class UserImpl implements UserSerive {

    @Autowired
    private UserMapper um;

    @Autowired
    private ProviceMapper pm;
    

    @Override
    public PageResult findAll(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<Users> page =  um.findAll();
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public void add(Users users) {
        users.setProvinceId(users.getCounty());
        um.add(users);
    }

    @Override
    public void edit(Users users) {
        um.edit(users);
    }
    

    @Override
    public Users findById(Integer id) {
        System.out.println(um.findId(id));
        return um.findId(id);
    }

    @Override
    public void del(Integer[] ids) {
        for (Integer id : ids) {
            um.del(id);
        }
    }

    @Override
    public List<Provice> findPCC(Integer id) {
        List<Provice> provices = pm.selectByOne(id);
        return provices;
    }

    @Override
    public Integer findParent(Integer id) {
        return pm.findParent(id);
    }
}
