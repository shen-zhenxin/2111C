package demo.serviceImpl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import demo.entity.PageResult;
import demo.entity.QueryPageBean;
import demo.mapper.PeopleMapper;
import demo.pojo.People;
import demo.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeopleImpl implements PeopleService {

    @Autowired
    private PeopleMapper pm;


    @Override
    public PageResult findAll(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<People> page =  pm.findAll();
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public void add(People people) {
        pm.add(people);
    }

    @Override
    public void edit(People people) {
        pm.edit(people);
    }

    @Override
    public void del(Integer id) {
        pm.del(id);
    }

    @Override
    public People findById(Integer id) {
        return pm.findId(id);
    }
}
