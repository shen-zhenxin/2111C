package demo.mapper;

import com.github.pagehelper.Page;
import demo.pojo.People;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface PeopleMapper extends Mapper<People> {

    @Select("select * from people")
    Page<People> findAll();

    @Delete("delete from people where id=#{id}")
    void del(Integer id);

    @Select("select * from people where id = #{id}")
    People findId(Integer id);

    @Update("update people set name=#{name},birthday=#{birthday} where id = #{id}")
    void edit(People people);

    @Insert("insert into people values(null,#{name},#{birthday})")
    void add(People people);
}
