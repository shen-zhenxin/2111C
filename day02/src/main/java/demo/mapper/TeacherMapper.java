package demo.mapper;

import demo.pojo.Dto;
import demo.pojo.Teacher;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@org.apache.ibatis.annotations.Mapper
public interface TeacherMapper extends Mapper<Teacher> {

    List<Dto> findAll(Map map);

    int add(Teacher teacher);
}
