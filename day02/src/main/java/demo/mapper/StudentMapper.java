package demo.mapper;

import demo.pojo.Student;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface StudentMapper extends Mapper<Student> {

    @Update("update student set tid=#{tid},`show`=1 where sid=#{sid}")
    void set(@Param("sid") Integer id, @Param("tid") Integer i);

    @Select("select * from student where `show`=0")
    List<Student> findStudent();

}
