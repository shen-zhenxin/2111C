package demo.mapper;

import demo.pojo.Provice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProviceMapper extends tk.mybatis.mapper.common.Mapper<Provice> {

    @Select("select * from province where parent_id = #{id}")
    List<Provice> selectByOne(Integer id);

    @Select("select parent_id as parentId from province where id = #{id}")
    Integer findParent(Integer id);
}
