package demo.service;

import com.github.pagehelper.PageInfo;
import demo.entity.PageResult;
import demo.entity.QueryPageBean;
import demo.pojo.People;

import java.util.List;

public interface PeopleService {
    PageResult findAll(QueryPageBean queryPageBean);

    void add(People people);

    void edit(People people);

    void del(Integer id);

    People findById(Integer id);
}
