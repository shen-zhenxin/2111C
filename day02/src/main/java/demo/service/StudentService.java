package demo.service;

import demo.pojo.Student;
import demo.pojo.Teacher;

import java.util.List;

public interface StudentService {

    void add(Teacher teacher, Integer[] ids);

    List<Student> findStudent();

}
