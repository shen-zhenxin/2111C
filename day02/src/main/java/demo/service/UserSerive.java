package demo.service;

import demo.entity.PageResult;
import demo.entity.QueryPageBean;
import demo.pojo.People;
import demo.pojo.Provice;
import demo.pojo.Users;

import java.util.List;

public interface UserSerive {
    PageResult findAll(QueryPageBean queryPageBean);

    void edit(Users users);

    void add(Users users);

    Users findById(Integer id);

    void del(Integer[] ids);

    List<Provice> findPCC(Integer id);

    Integer findParent(Integer id);
}
