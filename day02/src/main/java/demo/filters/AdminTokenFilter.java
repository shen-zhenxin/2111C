package demo.filters;

import com.alibaba.fastjson.JSON;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@WebFilter(filterName="AdminTokenFilter",urlPatterns = "/*")
public class AdminTokenFilter extends GenericFilterBean {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("进过滤器了");
        res.setCharacterEncoding("utf-8");
        res.setContentType("application/json");
        Map map = new HashMap();
        map.put("code","123456");
        res.getOutputStream().write(JSON.toJSONString(map).getBytes());
    }
}