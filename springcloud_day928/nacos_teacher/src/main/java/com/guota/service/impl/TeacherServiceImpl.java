package com.guota.service.impl;

import com.guota.mapper.TeacherMapper;
import com.guota.pojo.Teacher;
import com.guota.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherMapper teacherMapper;
    @Override
    public void editTea(Teacher teacher) {
        teacherMapper.updateByPrimaryKeySelective(teacher);
    }

    @Override
    public Teacher findOneTea(Integer id) {
        return teacherMapper.selectByPrimaryKey(id);
    }

    @Override
    public void insertTea(Teacher teacher) {
        teacherMapper.insertSelective(teacher);
    }

    @Override
    public void del(Integer id) {
        teacherMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Teacher findById(Integer id) {
        return teacherMapper.selectByPrimaryKey(id);
    }
}
