package com.guota.mapper;

import com.guota.pojo.Teacher;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface TeacherMapper extends Mapper<Teacher> {
}
