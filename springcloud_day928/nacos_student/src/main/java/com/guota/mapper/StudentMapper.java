package com.guota.mapper;

import com.guota.pojo.Student;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface StudentMapper extends Mapper<Student> {

    @Update("update student set tid=0 where id = #{id}")
    void edit(Integer id);
}
