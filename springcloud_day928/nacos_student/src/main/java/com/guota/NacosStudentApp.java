package com.guota;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NacosStudentApp {

    public static void main(String[] args) {
        SpringApplication.run(NacosStudentApp.class);
    }


    // httpclient
    // restTemplate



}