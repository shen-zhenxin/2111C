package com.guota.controller;

import com.guota.pojo.Result;
import com.guota.pojo.Student;
import com.guota.pojo.Teacher;
import com.guota.service.StudentService;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import sun.reflect.generics.tree.ReturnType;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private RestTemplate restTemplate;  // 请求

    @Autowired
    private LoadBalancerClient loadBalancerClient;  // 自动获取地址

    @Autowired
    private StudentService studentService;
    @PostMapping("/add")
    public Result add(@RequestBody Student student){
        studentService.insertStu(student);
        ServiceInstance serviceInstance = loadBalancerClient.choose("nacos-teacher");
        String url1 = serviceInstance.getUri()+"/teacher/findOne?id="+1;
        Teacher teacher =  restTemplate.getForObject(url1,Teacher.class);
        Integer sid = student.getId();
        String sid1 = teacher.getSid();
        String url2 = serviceInstance.getUri()+"/teacher/edit";
        Teacher teacher1=new Teacher();
        teacher1.setId(1);
        teacher1.setSid(sid1+","+sid);
        restTemplate.postForObject(url2,teacher1,Teacher.class);
        return new Result(true,"成功!!!");
    }
    @GetMapping("/edit")
    public Result edit(Integer id){
        studentService.findByTid(id);
        return new Result(true,"成功!!!");
    }
    @GetMapping("/findAll")
    public Student findAll(Integer id){
        return studentService.findAll(id);
    }

}
